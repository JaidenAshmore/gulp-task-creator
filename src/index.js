import nodemon from 'gulp-nodemon';

/**
 * Gulp task builder used for creating gulp tasks.
 *
 * This simplifies the process of creating tasks that automatically clean itself before starting or watching for changes
 * for it's files.
 */
export class GulpTaskBuilder {
    /**
     * @param gulp is the gulp executable. For some reason you need to pass it in.
     */
    constructor(gulp) {
        this._gulp = gulp;
        this._fileTaskSrc = {};
    }

    /**
     * Create a task that when triggered executes a given function
     *
     * @param {string} name of the task for gulp to use
     * @param {string[]} prerequisites task names to be completed before this can be run
     * @param {function} [taskToDo] will be executed after the prerequisite tasks are run
     * @returns {GulpTaskBuilder}
     */
    newTask(name, prerequisites, taskToDo = (() => {})) {
        this._gulp.task(name, prerequisites, taskToDo);

        return this;
    }

    /**
     * Create a task that will take a given src files, process these files and then possibly move them to a destination.
     * <p>
     *     This also creates a <taskname>-no-clean where we do not first clean the destination folder before executing.
     * </p>
     *
     * @param {string} name of the task for gulp to use
     * @param {string[]} prerequisites task names to be completed before this can be run
     * @param {string[]} src file locator
     * @param {string} [dest] to output the files if necessary
     * @param {function} [taskToDo] will be executed after the prerequisite tasks are run
     * @returns {GulpTaskBuilder}
     */
    newFileTask(name, prerequisites, src, dest, taskToDo = ((gulpTask ) => {return gulpTask;})) {
        var fileProcessing = () => {
            var processedSrc = taskToDo(this._gulp.src(src));
            if (dest) {
                return processedSrc.pipe(this._gulp.dest(dest));
            } else {
                return processedSrc;
            }
        };

        this._gulp.task(name, prerequisites, fileProcessing);

        this._fileTaskSrc[name] = src;

        return this;
    }

    /**
     * Creates a task that when run will watch any of the given tasks src files and re-run them if a change is made.
     * <p>
     * NOTE: this will not monitor any second level task source files.
     * </p>
     *
     * @param {string} name of the task for gulp to use
     * @param {string[]} tasks whose source will be monitored to change to run again
     * @returns {GulpTaskBuilder}
     */
    newWatchableTask(name, tasks) {
        this._gulp.task(name + '-prereq', tasks, () => {});

        this.newTask(name, [name + '-prereq'], () => {
            tasks.forEach((task) => {
                if (this._fileTaskSrc[task]) {
                    console.log("Watching files: ", this._fileTaskSrc[task], " and will do '" + task + "' if they change");
                    this._gulp.watch(this._fileTaskSrc[task], [task]);
                }
            });
        });

        return this;
    }

    /**
     * Create a gulp task that allows for running a script using nodemon.
     *
     * @param {string} name of the task for gulp to use
     * @param {string[]} tasks which will be run before starting the server as well as when there is a change to the files
     * @param {string[]} srcToWatch would monitor these and re-run the server on changes
     * @param {string} script that would be executed to start the server
     * @returns {GulpTaskBuilder}
     */
    newNodemonTask(name, tasks, srcToWatch, script) {
        this._gulp.task(name, tasks, () => {
            return nodemon({
                script: script,
                ext: '*',
                watch: srcToWatch,
                tasks: tasks
            });
        });

        return this;
    }

    /**
     * Helper static function for creating the task builder.
     *
     * @param gulp is the gulp executable. For some reason you need to pass it in.
     * @returns {GulpTaskBuilder}
     */
    static newBuilder(gulp) {
        return new GulpTaskBuilder(gulp);
    }
}
