'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Dog = exports.Dog = function () {
    function Dog() {
        _classCallCheck(this, Dog);
    }

    _createClass(Dog, [{
        key: 'constuctor',
        value: function constuctor() {
            this._name = 'spot';
        }
    }, {
        key: 'bark',
        value: function bark() {
            console.log('woof');
        }
    }]);

    return Dog;
}();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImRvZy5qcyJdLCJuYW1lcyI6WyJEb2ciLCJfbmFtZSIsImNvbnNvbGUiLCJsb2ciXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7SUFDYUEsRyxXQUFBQSxHOzs7Ozs7O3FDQUNJO0FBQ1QsaUJBQUtDLEtBQUwsR0FBYSxNQUFiO0FBQ0g7OzsrQkFFTTtBQUNIQyxvQkFBUUMsR0FBUixDQUFZLE1BQVo7QUFDSCIsImZpbGUiOiJkb2cuanMiLCJzb3VyY2VzQ29udGVudCI6WyJcbmV4cG9ydCBjbGFzcyBEb2cge1xuICAgIGNvbnN0dWN0b3IoKSB7XG4gICAgICAgIHRoaXMuX25hbWUgPSAnc3BvdCc7XG4gICAgfVxuXG4gICAgYmFyaygpIHtcbiAgICAgICAgY29uc29sZS5sb2coJ3dvb2YnKTtcbiAgICB9XG59XG4iXX0=