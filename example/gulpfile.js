var gulp = require("gulp");
var babel = require("gulp-babel");
var del = require('del');

var GulpTaskBuilder = require('../dist/index').GulpTaskBuilder;

GulpTaskBuilder.newBuilder(gulp)
    .newTask('clean', [], function() {
        return del('./dist');
    })
    .newFileTask('move', [], ["./src/move/**/*.*"], './dist/move', function(gulpTask) {
        return gulpTask;
    })
    .newFileTask('es6', [], ["./src/es6/**/*.*"], './dist/es6', function(gulpTask) {
        return gulpTask.pipe(babel());
    })
    .newFileTask('jsx', [], ["./src/views/**/*.jsx"], './dist/views', function(gulpTask) {
        return gulpTask.pipe(babel());
    })
    .newFileTask('html', [], ["./src/html/**/*.*"], './dist/html')
    .newFileTask('app', [], ["./src/index.js"], './dist/')
    .newTask('default', ['move', 'es6', 'html', 'jsx', 'app'])
    .newWatchableTask('watch', ['move', 'es6', 'html', 'jsx', 'app'])
    .newNodemonTask('server', ['default'], 'src', './dist');